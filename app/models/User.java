package models;


import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;

import play.data.format.*;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;

@Entity
public class User extends Model{
	public static Finder<Long, User> find = new Finder<Long, User>(Long.class, User.class);
	
	@Id
	public Long id;
	
	@Required
	public String email;
	
	@Required
	public String name;
	
	@Required
	public String androidId;

	@Required
	public String password;
	
	public double cash_amount;
	
	public static List<User> all(){
		return find.all();
	}
	
	public static void create(User user){
		user.save();
	}
	
	public static User authenticate(String email, String password){
		return find.where()
	            .eq("email", email)
	            .eq("password", password)
	            .findUnique();
		
		
	}
	
	public static User findByEmail(String email){
		return find.where()
				.eq("email", email)
				.findUnique();
	}
	

}
