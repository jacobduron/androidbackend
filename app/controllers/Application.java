package controllers;

import play.*;
import play.libs.Json;
import play.mvc.*;
import models.*;
import play.data.Form;
import play.data.validation.Constraints.*;
import play.data.*;
import helpclasses.Login;
import views.*;
import views.html.*;



public class Application extends Controller{

	public static Result index() {
        return ok("hi!");
    }
	
	public static Result register(){
    	return ok(
    			register.render(Form.form(User.class))
    			);
    }
	
	
	static Form<Login> loginForm = Form.form(Login.class);
	public static Result login(){
		return ok(
				login.render(loginForm)
				
				);

    }
	
	
	
}
