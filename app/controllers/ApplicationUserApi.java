package controllers;



import org.codehaus.jackson.node.ObjectNode;

import play.*;
import play.libs.Json;
import play.mvc.*;
import models.*;
import play.data.Form;
import play.data.validation.Constraints.*;
import play.data.*;
import helpclasses.Login;
import views.*;
import views.html.*;

public class ApplicationUserApi extends Controller {
  
    public static Result index() {
        return redirect(routes.ApplicationUserApi.login());
    }
    
    
    static Form<User> regForm = Form.form(User.class);
    public static Result register(){
    	return ok(
    			register.render(Form.form(User.class))
    			);
    }
    
    
    
    
    
    public static Result userRegistration(){
    	Form<User> filledForm = Form.form(User.class).bindFromRequest();
    	if(filledForm.hasErrors()){
    		return badRequest(register.render(filledForm));
		}else{
			User newUser = filledForm.get();
			newUser.save();
			ObjectNode result = Json.newObject();
			result.put("email", newUser.email);
			result.put("name", newUser.name);
			result.put("cash_amount", newUser.cash_amount);
			
			return ok(result);
		}
    	
    	
    	
    }
    
    
    
    
    
    
    static Form<Login> loginForm = Form.form(Login.class);
    public static Result login(){
		return ok(
				login.render(loginForm)
				
				);

    }
    
    
    public static Result authenticate(){
    	 Form<Login> loginFilled = loginForm.bindFromRequest();
    	 ObjectNode result = Json.newObject();
		if(loginFilled.hasErrors()){
			result.put("email", "badRequest");
			
			return badRequest(result);
		}else{
			User user = User.findByEmail(loginFilled.get().email);
			
			result.put("email", user.email);
			result.put("name", user.name);
			result.put("cash_amount", user.cash_amount);
			return ok(result);
		}
    	
    }
  


  
}
