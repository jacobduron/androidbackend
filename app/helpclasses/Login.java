package helpclasses;
import models.User;


public class Login {
	public String email;
	public String password;
	
	public String validate(){
		if(User.authenticate(email, password) == null){
			return "Invalid email or password";
		}else{
			return null;
		}
	}
	
}
